# ChacoCanyon - A graphical Java game

A simple text game written in Java, and supported by a graphical user interface.

They player is in charge of the Canyon, and is responsible for its upkeep. They can choose to use their resources to expand their land, to feed their people, or to recuperate from natural disasters that might randomly befall them. 

If the player survives long enough, they win. If their countries economic status falls due to drought, plague, or infestation, they lose.

## Simulator

This contains the methods, random occurences, and text to display to the user. It's the logical back end of the program

## ChacoCanyon

This contains the graphical interface, and the user input, as well as some additional text input/output that the simulator doesn't handle

## Tester

A tester file to ensure that certain methods are working properly.
