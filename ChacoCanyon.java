
import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author gusty
 */
public class ChacoCanyon extends JPanel implements ActionListener {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        JFrame window = new JFrame("Chaco Canyon");
        ChacoCanyon chaco = new ChacoCanyon();
        window.setContentPane( chaco );
        window.pack();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setLocation(150,100);
        window.setVisible(true);  
    }
    
    private boolean doDebug;  // turns on debug print information

    // Define widgets for the GUI
    private final JTextArea chacoTranscript;   
    private final JComboBox<String> combobox;   
    private final JTextField landAcres;   
    private final JTextField grainToFeed;    
    private final JTextField acresToPlant;    
    private final JCheckBox debug;   
    
    // Define chacoCanyon
    ChacoCanyonSimulation chacoCanyon;

    /**
     * This constructor adds several GUI components to the panel and sets
     * itself up to listen for action events from some of them.
     */
    public ChacoCanyon() {
        
        doDebug = false;

        setBorder(BorderFactory.createLineBorder(Color.GRAY, 3));
        setBackground(Color.WHITE);

        setLayout(new GridLayout(1, 2, 3, 3)); // 1 row, 2 col, hor/ver gaps 3
            // I will put the chacoTranscript area for Chaco Canyon results 
            // in the right half of the panel.
            // The left half will be occupied by a grid of lines. Each line
            // contains a component and a label for that component.

        chacoTranscript = new JTextArea();
        chacoTranscript.setEditable(false);
        chacoTranscript.setMargin(new Insets(4, 4, 4, 4));
        JPanel left = new JPanel();
        left.setLayout(new GridLayout(6, 2, 10, 10)); // 6 row, 2 col, hor 5, ver 10
        left.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        add(left);
        add(new JScrollPane(chacoTranscript));

        JLabel lab = new JLabel("Push Button:   ", JLabel.RIGHT);
        left.add(lab);
        JButton b = new JButton("Simulate One Year");
        b.addActionListener(this);
        left.add(b);

        lab = new JLabel("Land Transactions:   ", JLabel.RIGHT);
        left.add(lab);
        combobox = new JComboBox<String>();
        combobox.addItem("Buy Land");
        combobox.addItem("Sell Land");
        combobox.addActionListener(this);
        left.add(combobox);

        lab = new JLabel("Checkbox:   ", JLabel.RIGHT);
        left.add(lab);
        debug = new JCheckBox("Debug");
        debug.addActionListener(this);
        left.add(debug);

        lab = new JLabel("Acres of Land:   ", JLabel.RIGHT);
        left.add(lab);
        landAcres = new JTextField("0");
        landAcres.addActionListener(this);
        left.add(landAcres);

        lab = new JLabel("Grain to Feed:   ", JLabel.RIGHT);
        left.add(lab);
        grainToFeed = new JTextField("0");
        grainToFeed.addActionListener(this);
        left.add(grainToFeed);
        
        lab = new JLabel("Acres to plant:   ", JLabel.RIGHT);
        left.add(lab);
        acresToPlant = new JTextField("0");
        acresToPlant.addActionListener(this);
        left.add(acresToPlant);
        
        chacoCanyon = new ChacoCanyonSimulation(1000, 100, 1000, 20, 3000);

        post(chacoCanyon.introMessage());
    }

    private void post(String message) { // add a line to the chacoTranscript
        chacoTranscript.append(message + "\n");
        chacoTranscript.setCaretPosition(chacoTranscript.getDocument().getLength());
    }

    /**
     * Respond to an ActionEvent from one of the GUI components in the panel.
     * In each case, a message about the event is posted to the chacoTranscript.
     * (This method is part of the ActionListener interface.)
     */
    public void actionPerformed(ActionEvent evt) {
        Object target = evt.getSource(); // which component produced this event?
        int grainToFeed = 0;
        int acresToBuy = 0;
        int acresToSell = 0;
        int acresToPlant = 0;
        if (target instanceof JButton) { // Simulate a year
            if (debug.isSelected()) {
                doDebug = true;
            }
            else {
                doDebug = false;
            }

            Object item = combobox.getSelectedItem();
            if (item.equals("Buy Land")) {
                acresToBuy = Integer.parseInt(this.landAcres.getText());
            } else if (item.equals("Sell Land")) {
                acresToSell = Integer.parseInt(this.landAcres.getText());
            }
            grainToFeed = Integer.parseInt(this.grainToFeed.getText());
            acresToPlant = Integer.parseInt(this.acresToPlant.getText());
            if (doDebug) {
                post("Run Button was clicked");
                post("Item \"" + item + "\" selected\nfrom pop-up menu.");
                post("File Names Entered");
                post("acresToBuy: " + acresToBuy);
                post("acresToSell: " + acresToSell);
                post("grainToFeed: " + grainToFeed);
            }
            
            post("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
            post(chacoCanyon.simulateYear(acresToBuy, acresToSell, acresToPlant, grainToFeed));
            post("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");

        } else if (target instanceof JTextField) {
            if (doDebug) {
                post("Pressed return in TextField\nwith contents:\n    "
                    + evt.getActionCommand());
            }
        } else if (target instanceof JCheckBox) {
            if (((JCheckBox) target).isSelected())
                if (doDebug) {
                    post("Checkbox was turned on.");
                }
            else
                if (doDebug) {
                    post("Checkbox was turned off.");
                }
        } else if (target == combobox) {
            if (doDebug) {
                Object item = combobox.getSelectedItem();
                post("Item \"" + item + "\" selected\nfrom pop-up menu.");
            }
        }
    }
}
