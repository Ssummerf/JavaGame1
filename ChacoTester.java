import java.util.*;

public class ChacoTester {

	public static void main(String Args[]){
		Scanner keyboard = new Scanner(System.in);
		
		
		
		ChacoCanyonSimulation Alpha = new ChacoCanyonSimulation(1000, 100, 1000, 20, 3000);
		int acresToBuy;
		int acresToSell;
		int acresToPlant;
		int grainToFeed;
		
		System.out.println(Alpha.introMessage());
		System.out.println(Alpha.endMessage());
		while(Alpha.lost != true){
		
		System.out.println();
		System.out.print("Enter acres to buy: ");
		acresToBuy = keyboard.nextInt();
		
		System.out.print("Enter acres to sell: ");
		acresToSell = keyboard.nextInt();
		
		System.out.print("Enter acres to plant: ");
		acresToPlant = keyboard.nextInt();
		
		System.out.print("Enter grain to feed: ");
		grainToFeed = keyboard.nextInt();
		System.out.println();
		
		System.out.println(Alpha.simulateYear(acresToBuy, acresToSell, acresToPlant, grainToFeed));
		}
	}
}
