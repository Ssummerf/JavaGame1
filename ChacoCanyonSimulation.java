import java.util.*;

/**
 * Chaco Canyon game to be run in a given GUI. Can also be used in a regular
 * static void main, but to less of an effect. There are many instance variables to simplify
 * the calculations involved during the class.
 * 
 * The abundance of instance variables is partially due to the format. Instead of returning
 * a several variables by the methods, they are instead simply void methods that change
 * the instance variables.
 * 
 * This results a wall of text in the initial lines of code, but much smoother coding
 * throughout the program.
 * @author Scott Summerford
 *
 */
public class ChacoCanyonSimulation {

	private int year; private int villagers;
	private int land; private int bushels;
	private int landvalue; private int starved;
	private int leavers; private int joiners;
	private int destroyed; private int bushelsgained;
	private int randomharvest; private String ErrorMessage;
	private int firsttime; public boolean lost;
	
	/**
	 * Constructor of the class, and sets the initial values of all of the instance
	 * variables. All values are initially set to the given starting value for the
	 * first yearly report.
	 * @param year
	 * @param villagers
	 * @param land
	 * @param landvalue
	 * @param bushels
	 */
	public ChacoCanyonSimulation(int year, int villagers, int land, int landvalue, int bushels){
		this.year = year;
		this.bushels = bushels;
		this.villagers = villagers;
		this.land = land;
		this.landvalue = landvalue;
		this.starved = 0; this.leavers = 0; this.joiners = 0;
		this.destroyed = 0;
		this.bushelsgained = 0;
		this.randomharvest = 3;
		this.firsttime = 0;
		this.lost = false;
	}
	
	/**
	 * Returns a string of the intro message.
	 * @return
	 */
	public String introMessage(){
		String s = " Congratulations, you are the newest leader of ancient Chaco Canyon, \n elected for a ten-year term of office. Your duties are to dispense food,\n direct farming, and buy and sell land as needed to support your people.\n Watch out for rat infestations and the drought! Maize is the general \n currency, measured in bushels. The following will help you in your \n decisions: \n • Each person needs at least 20 bushels of maize per year to survive. \n • Each person can farm at most 10 acres of land. \n • It takes 2 bushels of maize to farm an acre of land. \n • The market price for land fluctuates yearly. \n \n Rule wisely and you will be showered with appreciation at the end of \n your term. Rule poorly and you will be kicked out of office! \n";
		return s; 
	}
	
	/**
	 * Creates a string out of all of the variables needed for the yearly report
	 * This is what is returned in simulateyear, as keeping the calculations and 
	 * the wall of text separate makes the program more reader friendly.
	 * @return
	 */
	public String endMessage(){
		String s = "";
		s +=("O great Chaco leader, I beg to report to you, \n");
		s +=("In the year " + year + ", " + starved + " people starved to death, " + leavers + " people left our nation, \n");
		s +=(joiners + " people joined our nation \n");
		s +=("The population is now " + villagers + " \n");
		s +=("The nation now owns " + land + " acres. \n");
		s +=("We harvested " + bushelsgained + " bushels at " + randomharvest + " bushels per acre. \n");
		s +=("Rats destroyed  " + destroyed + " leaving " + bushels + " in storage. \n");
		s +=("Land is currently worth " + landvalue + " bushels per acre.");
		return s;
	}
	
	/**
	 * Simulates the year with the users given variables. All of the calculations
	 * except for the resulting bushel numbers are down by other methods in the class.
	 * 
	 * This method also checks for any errors before it runs the simulation, and prints
	 * the error if one is found, without executing the simulation.
	 * 
	 * If it is the first simulation, it automatically goes to the end message without
	 * calculations, since all the values are already set.
	 * 
	 * If the game has been lost, the game will not execute, and ask the user to restart
	 * 
	 * @param acresToBuy
	 * @param acresToSell
	 * @param acresToPlant
	 * @param grainToFeed
	 * @return
	 */
	public String simulateYear(int acresToBuy, int acresToSell, int acresToPlant, int grainToFeed){
		
		if(firsttime ==0){
			firsttime = 1;
			return this.endMessage();
		}
		
		if(lost)
			return "You lost the game, please restart and try again";
		
		starved = 0;
		destroyed = 0;
		joiners = 0;
		leavers = 0;
		bushelsgained = 0;
		
		int original = (int) (0.45 * villagers);
			
		boolean Error = this.ErrorChecker( acresToBuy, acresToSell, acresToPlant, grainToFeed);
		
		if(Error == false){

		this.drought();
		this.Starvation(grainToFeed);
		this.Immigration();
		this.Infestation();
		
		bushels -= acresToBuy * landvalue;
		bushels += acresToSell * landvalue;

		this.Harvest(acresToPlant);
		this.LandValue();
		
		bushels -= destroyed;
		bushels -= grainToFeed;
		bushels -= 2 * acresToPlant;
		bushels += bushelsgained;
		
		land -= acresToSell;
		land += acresToBuy;
		
		year += 1;
		
		int finalcheck = starved + leavers;
		if(finalcheck > original){
			String x = (starved + " people starved and " + leavers + " people left this year \nFar too many for a great leader. Goodbye.");
			this.lost = true;
			return x;
		}else
		return this.endMessage();
		
		}else{
			return ErrorMessage;
		}
	}
	
	/**
	 * Creates a random number between 1 and 100. If it is lower than 15, a drought occurs
	 * Killing 50% of the population and ultimately ending the game.
	 */
	public void drought(){
		int droughtchance = (int)(Math.random()*99)+1;
		if(droughtchance < 15){
			starved = (int)(0.3*villagers);
			leavers = (int)(0.2*villagers);
			villagers = (int)villagers / 2;
		}
	}
	
	/**
	 * Calculates how many people starve based on the bushels given to feed them.
	 * Each person needs 20 bushels to live through the year. Rounds to whole person.
	 * 
	 * @param grainToFeed
	 */
	public void Starvation(int grainToFeed){
		int neededmaize = 20 * villagers;
		int actualmaize = grainToFeed;
		
		if(actualmaize < neededmaize){
			int difference = neededmaize - actualmaize;
			difference = difference / 20;
			villagers -= difference;
			starved += difference;
		}	
	}
	
	/**
	 * Calculates how many immigrants come into the country that year.
	 * If any villagers have starved, none will come.
	 */
	public void Immigration(){
		if(starved == 0){
			double randomjoin = Math.random() * 1;
			randomjoin = ((20 * (land + bushels)) / ((100 * villagers) + 1)) * randomjoin;
			joiners = (int)randomjoin;
			villagers += joiners;
		}
	}
	
	/**
	 * Rolls a random number between 1 and 2. If the number is two
	 * another random number is generated between 4% and 40% and that much grain
	 * will be destroyed
	 */
	public void Infestation(){
		int randominf = (int)(Math.random() * 1) + 1;
		if(randominf == 2){
			int loss = (int)(Math.random() * 36) + 4;
			destroyed = (int)(loss / 100) * bushels;
		}
	}
	
	/**
	 * Determines how much grain is harvested based on the acres to be planted,
	 * multiplied by a random number between 1 and 8
	 * @param acresToPlant
	 */
	public void Harvest(int acresToPlant){
	    randomharvest = (int)((Math.random()*8) + 1);
		bushelsgained = randomharvest * acresToPlant;
	}
	
	/**
	 * Gives a random value for the land between 17 and 26
	 */
	public void LandValue(){
		landvalue = (int)(Math.random() * 9) + 17;
	}
	
	/**
	 * Checks for any errors based on the values given. 
	 * IF: A user tries to sell more land than they have
	 * A user tries to buy more land than they can afford
	 * A user tries to feed more bushels than they have
	 * A user tries to plant more land than they have
	 * Or, a user does not have enough bushels to fully complete their action
	 * 
	 * An error message is printed and the program will not execute.
	 * 
	 * @param acresToBuy
	 * @param acresToSell
	 * @param acresToPlant
	 * @param grainToFeed
	 * @return
	 */
	public boolean ErrorChecker(int acresToBuy, int acresToSell, int acresToPlant, int grainToFeed){
		boolean Error = false;
		
		if(acresToSell > land){
			Error = true;
			ErrorMessage =("\n You cannot sell more land than you have. Try Again. \n");
		}
		
		int cost = landvalue * acresToBuy;
		
		if(cost > bushels){
			Error = true;
			ErrorMessage = ("\n You cannot buy that much land with your current amount of bushels. Try Again. \n");
		}
		
		if(grainToFeed > bushels){
			Error = true;
			ErrorMessage = ("\n You cannot feed your people more bushels than you have. Try Again. \n");
		}
		
		if(acresToPlant > land){
			Error = true;
			ErrorMessage = ("\n You cannot plant more land than you have. Try Again. \n");
		}
		
		cost = acresToPlant * 2;
		
		if(cost > bushels){
			Error = true;
			ErrorMessage = ("\n You do not have enough bushels in storage to seed that much land. Try Again \n");
		}
		
		int sum = cost + grainToFeed + (acresToBuy * this.landvalue);
		if(sum > bushels){
			Error = true;
			ErrorMessage =("\n You do not have enough bushels to complete this action");
		}
		return Error;
	}
}
